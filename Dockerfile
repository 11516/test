FROM bitnami/git:latest

RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git

WORKDIR service-a/

FROM golang:1.17

RUN export GO111MODULE=on

RUN export GOPROXY=https://goproxy.cn

RUN go mod tidy

RUN go build

CMD ["./service", "-a", "--file", "config.yaml"]